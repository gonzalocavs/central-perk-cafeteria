package centralperk;

import centralperk.cafe.Expresso;
import centralperk.cafe.Normal;
import centralperk.cafe.Ristretto;
import centralperk.te.negro;
import centralperk.te.rojo;
import centralperk.te.verde;
import java.util.Scanner;

public class CentralPerk {

    public static void main(String[] args) {

        Bebida[] pedido = new Bebida[5];

        boolean otroPedido = true;
        int siguiente = 0;
        String nombre;
        Scanner sc = new Scanner(System.in);

        System.out.println("Elige un personaje de la serie");

        nombre = sc.next();

        System.out.println("\n");

        if (nombre.equals("Joey")) {
            System.out.println("How you doin'? " + nombre);
        }

        if (nombre.equals("Chandler")) {
            System.out.println("Hola Sr. Bing");
        }

        if (nombre.equals("Rachel")) {
            System.out.println("Ya pasaste por la peluquerìa y ahora Gunter te regala una bebida. Tu eliges " + nombre);
        }

        if (nombre.equals("Phoebe")) {
            System.out.println("Hola Regina Falange");
        }

        if (nombre.equals("Ross")) {
            System.out.println("Hola Doctor Ross ¿Cómo está Carol?");
        }
        if (nombre.equals("Monica")) {
            System.out.println("Hola Sra. Bing");
        }
    


        
    


while (otroPedido) {

            System.out.println("¿Qué vas a llevar?\n"
                    + "1)Café\n"
                    + "2)Té\n");
            Scanner entrada = new Scanner(System.in);
            int clase = entrada.nextInt();

            if (clase == 1) {

                System.out.println(""
                        + "1-Expresso\n"
                        + "2-Ristretto\n"
                        + "3-Normal\n");
                Scanner entradacafe = new Scanner(System.in);
                int tipoCafe = entradacafe.nextInt();
                switch (tipoCafe) {
                    case 1:
                        pedido[siguiente] = new Expresso();
                        break;
                    case 2:
                        pedido[siguiente] = new Ristretto();
                        break;
                    case 3:
                        pedido[siguiente] = new Normal();
                        break;
                }

                System.out.println("¿Cuántos sobres de azucar deseas?0-3");
                Scanner entradaazucar = new Scanner(System.in);
                int azucar = entradacafe.nextInt();
                pedido[siguiente].setAzucar(azucar);

                System.out.println("¿Cantidad de leche? 0-3");
                Scanner entradaleche = new Scanner(System.in);
                int leche = entradaleche.nextInt();
                pedido[siguiente].setLeche(leche);
            }

            if (clase == 2) {

                System.out.println(""
                        + "1-Negro\n"
                        + "2-Rojo\n"
                        + "3-Verde\n");
                Scanner entradacafe = new Scanner(System.in);
                int tipoTe = entradacafe.nextInt();
                switch (tipoTe) {
                    case 1:
                        pedido[siguiente] = new negro();
                        break;
                    case 2:
                        pedido[siguiente] = new rojo();
                        break;
                    case 3:
                        pedido[siguiente] = new verde();
                        break;
                }

                System.out.println("¿Cuántos sobres de azúcar deseas? 0-3");
                Scanner entradaazucar = new Scanner(System.in);
                int azucar = entradacafe.nextInt();
                pedido[siguiente].setAzucar(azucar);

                System.out.println("¿Cantidad de leche? 0-3");
                Scanner entradaleche = new Scanner(System.in);
                int leche = entradaleche.nextInt();
                pedido[siguiente].setLeche(leche);
            }

            System.out.println("¿Desea algo mas?\n"
                    + "1-Sí\n"
                    + "2-No\n");
            Scanner entradaOtroPedido = new Scanner(System.in);
            int otro = entradaOtroPedido.nextInt();
            if (otro == 1) {
                siguiente++;
            } else {
                otroPedido = false;
            }

        }

        System.out.println("\n");

        double subtotal = 0, total = 0;

        for (int i = 0; i < pedido.length; i++) {

            if (pedido[i] != null) {
                System.out.println(pedido[i]);
                subtotal = subtotal + pedido[i].getPrecio()
                        + pedido[i].getAzucar() * 5
                        + pedido[i].getLeche() * 5;

            }

        }
        total = subtotal * 1.21;
        System.out.println("Subtotal:" + subtotal);
        System.out.println("Total:" + total);

    }
}
