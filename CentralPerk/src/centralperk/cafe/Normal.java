
package centralperk.cafe;
import centralperk.Bebida;

public class Normal extends cafe{
    
    @Override
    public String getDescripcion() {
        return "Normal"; //Retorno la descripción
    }

    @Override
    public double getPrecio() {
        return 90; //Retorno el precio del café normal
    }
}


